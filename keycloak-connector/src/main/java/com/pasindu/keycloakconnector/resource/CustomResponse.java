package com.pasindu.keycloakconnector.resource;

public class CustomResponse {

    private int status;
    private String message;
    private String error;
    private Object body;

    public CustomResponse() {
    }

    public CustomResponse(int status, String message) {
        this.status = status;
        this.message = message;
    }

    public CustomResponse(int status, String message, String error) {
        this.status = status;
        this.message = message;
        this.error = error;
    }

    public CustomResponse(int status, String message, Object body) {
        this.status = status;
        this.message = message;
        this.body = body;
    }

    public Object getBody() {
        return body;
    }

    public void setBody(Object body) {
        this.body = body;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }
}
