package com.pasindu.keycloakimplbe.handler;

import com.pasindu.keycloakimplbe.resource.CustomResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class CustomRestExceptionHandler extends ResponseEntityExceptionHandler {

    private static final Logger logger = LoggerFactory.getLogger(CustomRestExceptionHandler.class);

    @ExceptionHandler(value = {HttpClientErrorException.Unauthorized.class})
    public ResponseEntity<Object> handleUnauthorizedException(HttpClientErrorException.Unauthorized ex, WebRequest request) {
        logger.error("Unauthorized Exception: ", ex.getMessage());
        return handleExceptionInternal(ex, new CustomResponse(HttpStatus.UNAUTHORIZED.value(), ex.getMessage(), HttpStatus.UNAUTHORIZED.getReasonPhrase()), new HttpHeaders(), HttpStatus.UNAUTHORIZED, request);
    }

    @ExceptionHandler(value = {HttpClientErrorException.Forbidden.class})
    public ResponseEntity<Object> handleForbiddenException(HttpClientErrorException.Forbidden ex, WebRequest request) {
        logger.error("Forbidden Exception: ", ex.getMessage());
        return handleExceptionInternal(ex, new CustomResponse(HttpStatus.FORBIDDEN.value(), ex.getMessage(), HttpStatus.FORBIDDEN.getReasonPhrase()), new HttpHeaders(), HttpStatus.FORBIDDEN, request);
    }

    @ExceptionHandler(value = {HttpClientErrorException.Conflict.class})
    public ResponseEntity<Object> handleConflictException(HttpClientErrorException.Conflict ex, WebRequest request) {
        logger.error("Conflict Exception: ", ex.getMessage());
        return handleExceptionInternal(ex, new CustomResponse(HttpStatus.CONFLICT.value(), ex.getMessage(), HttpStatus.CONFLICT.getReasonPhrase()), new HttpHeaders(), HttpStatus.CONFLICT, request);
    }

    @ExceptionHandler(value = {HttpServerErrorException.InternalServerError.class})
    public ResponseEntity<Object> handleConflictException(HttpServerErrorException.InternalServerError ex, WebRequest request) {
        logger.error("Internal Server Error: ", ex.getMessage());
        return handleExceptionInternal(ex, new CustomResponse(HttpStatus.INTERNAL_SERVER_ERROR.value(), ex.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR.getReasonPhrase()), new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR, request);
    }
}
