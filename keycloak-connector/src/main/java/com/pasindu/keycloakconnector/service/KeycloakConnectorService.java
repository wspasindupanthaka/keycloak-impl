package com.pasindu.keycloakconnector.service;

import com.pasindu.keycloakconnector.resource.request.CreateUserRequest;
import com.pasindu.keycloakconnector.resource.request.LoginRequest;
import com.pasindu.keycloakconnector.resource.request.UserInfoRequest;
import com.pasindu.keycloakconnector.resource.response.LoginResponse;
import com.pasindu.keycloakconnector.resource.response.UserInfoResponse;
import com.pasindu.keycloakconnector.resource.response.UsersListItem;

import java.util.List;


public interface KeycloakConnectorService {

    LoginResponse token(String realmName, LoginRequest loginRequest);

    int createUser(String realmName, String authorizationHeader, CreateUserRequest createUserRequest);

    UserInfoResponse userInfo(String realmName, UserInfoRequest userInfoRequest);

    UsersListItem[] usersList(String authorizationHeader, String realmName, String email);

    int sendExecuteActionEmail(String authorizationHeader, String userId, List<String> actions, String realmName);
}
