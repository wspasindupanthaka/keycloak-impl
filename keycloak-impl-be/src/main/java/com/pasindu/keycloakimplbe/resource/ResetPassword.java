package com.pasindu.keycloakimplbe.resource;

public class ResetPassword {

    private String email;

    public ResetPassword() {
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
