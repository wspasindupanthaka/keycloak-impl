package com.pasindu.keycloakconnector.controller;

import com.pasindu.keycloakconnector.resource.CustomResponse;
import com.pasindu.keycloakconnector.resource.request.CreateUserRequest;
import com.pasindu.keycloakconnector.resource.request.LoginRequest;
import com.pasindu.keycloakconnector.resource.request.UserInfoRequest;
import com.pasindu.keycloakconnector.resource.response.LoginResponse;
import com.pasindu.keycloakconnector.resource.response.UserInfoResponse;
import com.pasindu.keycloakconnector.resource.response.UsersListItem;
import com.pasindu.keycloakconnector.service.KeycloakConnectorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class KeycloakConnectorControllerImpl implements KeycloakConnectorController{

    @Autowired
    private KeycloakConnectorService keycloakConnectorService;

    @Override
    public ResponseEntity<LoginResponse> token(LoginRequest loginRequest, @PathVariable(value = "realmName") String realmName) {
        LoginResponse token = keycloakConnectorService.token(realmName, loginRequest);
        return ResponseEntity.status(HttpStatus.OK).body(token);
    }

    @Override
    public ResponseEntity<CustomResponse> createUser(@RequestHeader(value = "Authorization") String authorizationHeader,
                                                     @RequestBody CreateUserRequest createUserRequest,
                                                     @PathVariable(value = "realmName") String realmName) {
        int userCreated = keycloakConnectorService.createUser(realmName, authorizationHeader, createUserRequest);
        ResponseEntity<CustomResponse> body = ResponseEntity.status(HttpStatus.valueOf(userCreated)).body(new CustomResponse(userCreated, HttpStatus.valueOf(userCreated).getReasonPhrase()));
        return body;
    }

    @Override
    public ResponseEntity<UserInfoResponse> userInfo(@RequestBody UserInfoRequest userInfoRequest,
                                                     @PathVariable(value = "realmName") String realmName) {
        UserInfoResponse userInfoResponse = keycloakConnectorService.userInfo(realmName, userInfoRequest);
        return ResponseEntity.status(HttpStatus.OK).body(userInfoResponse);
    }

    @Override
    public ResponseEntity<UsersListItem[]> usersList(@RequestHeader(value = "Authorization") String authorizationHeader,
                                                     @PathVariable(value = "realmName") String realmName,
                                                     @RequestParam(value = "email", required = false) String email) {
        UsersListItem[] usersListItems = keycloakConnectorService.usersList(authorizationHeader, realmName,email);
        return ResponseEntity.status(HttpStatus.OK).body(usersListItems);
    }

    @Override
    public ResponseEntity<CustomResponse> sendExecuteActionEmail(@RequestHeader(value = "Authorization") String authorizationHeader,
                                                                 @PathVariable(value = "userId") String userId,
                                                                 @RequestBody List<String> actions,
                                                                 @PathVariable(value = "realmName") String realmName) {
        int emailSent = keycloakConnectorService.sendExecuteActionEmail(authorizationHeader, userId, actions, realmName);
        CustomResponse customResponse = new CustomResponse(emailSent, HttpStatus.valueOf(emailSent).getReasonPhrase());
        return ResponseEntity.status(HttpStatus.valueOf(emailSent)).body(customResponse);
    }
}

