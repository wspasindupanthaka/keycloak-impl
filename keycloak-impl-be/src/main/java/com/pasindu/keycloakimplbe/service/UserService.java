package com.pasindu.keycloakimplbe.service;

import com.pasindu.keycloakimplbe.resource.CreateUser;
import com.pasindu.keycloakimplbe.resource.CustomResponse;
import com.pasindu.keycloakimplbe.resource.ResetPassword;

public interface UserService {

    CustomResponse createUser(String authorizationHeader, CreateUser createUser);

    CustomResponse changePassword(String authorizationHeader);

    CustomResponse resetPassword(ResetPassword resetPassword);

    String getLoggedInUser(String authorizationHeader);

}
