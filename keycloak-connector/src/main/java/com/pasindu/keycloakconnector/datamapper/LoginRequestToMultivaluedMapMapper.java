package com.pasindu.keycloakconnector.datamapper;

import com.pasindu.keycloakconnector.resource.request.LoginRequest;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import java.util.Map;

public class LoginRequestToMultivaluedMapMapper {

    public static MultiValueMap<String, String> map(LoginRequest loginRequest) {

        MultiValueMap<String, String> map = new LinkedMultiValueMap<>();

        if (LoginRequest.REFRESH_TOKEN.equals(loginRequest.getGrant_type())) {
            map.add("grant_type", LoginRequest.REFRESH_TOKEN);
            map.add("refresh_token", loginRequest.getRefresh_token());
        } else {
            map.add("grant_type", LoginRequest.PASSWORD);
            map.add("username", loginRequest.getUsername());
            map.add("password", loginRequest.getPassword());
        }


        map.add("client_id", loginRequest.getClient_id());
        map.add("client_secret", loginRequest.getClient_secret());

        return map;
    }
}
