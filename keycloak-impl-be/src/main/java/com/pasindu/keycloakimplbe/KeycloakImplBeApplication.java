package com.pasindu.keycloakimplbe;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class KeycloakImplBeApplication {

	public static void main(String[] args) {
		SpringApplication.run(KeycloakImplBeApplication.class, args);
	}

}
