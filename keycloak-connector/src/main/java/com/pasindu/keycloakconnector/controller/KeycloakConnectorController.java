package com.pasindu.keycloakconnector.controller;

import com.pasindu.keycloakconnector.resource.*;
import com.pasindu.keycloakconnector.resource.request.CreateUserRequest;
import com.pasindu.keycloakconnector.resource.request.LoginRequest;
import com.pasindu.keycloakconnector.resource.request.UserInfoRequest;
import com.pasindu.keycloakconnector.resource.response.LoginResponse;
import com.pasindu.keycloakconnector.resource.response.UserInfoResponse;
import com.pasindu.keycloakconnector.resource.response.UsersListItem;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequestMapping("/auth")
public interface KeycloakConnectorController {

    @CrossOrigin(origins = "http://localhost:4200")
    @RequestMapping(value = "/realms/{realmName}/protocol/openid-connect/token", method = RequestMethod.POST
            , consumes = "application/x-www-form-urlencoded")
    ResponseEntity<LoginResponse> token(@ModelAttribute LoginRequest loginRequest, @PathVariable(value = "realmName") String realmName);

    @RequestMapping(value = "/admin/realms/{realmName}/users", method = RequestMethod.POST
            , consumes = "application/json")
    ResponseEntity<CustomResponse> createUser(@RequestHeader(value = "Authorization") String authorizationHeader,
                                              @RequestBody CreateUserRequest createUserRequest,
                                              @PathVariable(value = "realmName") String realmName);

    @RequestMapping(value = "/realms/{realmName}/protocol/openid-connect/userinfo", method = RequestMethod.POST
            , consumes = "application/x-www-form-urlencoded")
    ResponseEntity<UserInfoResponse> userInfo(@ModelAttribute UserInfoRequest userInfoRequest,
                                              @PathVariable(value = "realmName") String realmName);

    @RequestMapping(value = "/admin/realms/{realmName}/users", method = RequestMethod.GET)
    ResponseEntity<UsersListItem[]> usersList(@RequestHeader(value = "Authorization") String authorizationHeader,
                                              @PathVariable(value = "realmName") String realmName,
                                              @RequestParam(value = "email", required = false) String email);

    @RequestMapping(value = "/admin/realms/{realmName}/users/{userId}/execute-actions-email", method = RequestMethod.PUT, consumes = "application/json")
    ResponseEntity<CustomResponse> sendExecuteActionEmail(@RequestHeader(value = "Authorization") String authorizationHeader,
                                                          @PathVariable(value = "userId") String userId,
                                                          @RequestBody List<String> actions,
                                                          @PathVariable(value = "realmName") String realmName);

}
