package com.pasindu.keycloakimplbe.resource;

import java.util.List;
import java.util.Map;

public class CreateUser {

    private String firstName;
    private String lastName;
    private String email;
    private boolean enabled;
    private String username;
    private Map<String, String> attributes;
    private List<Credentials> credentials;

    public CreateUser() {
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Map<String, String> getAttributes() {
        return attributes;
    }

    public void setAttributes(Map<String, String> attributes) {
        this.attributes = attributes;
    }

    public List<Credentials> getCredentials() {
        return credentials;
    }

    public void setCredentials(List<Credentials> credentials) {
        this.credentials = credentials;
    }

    @Override
    public String toString() {
        return "CreateUser{" +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", email='" + email + '\'' +
                ", enabled=" + enabled +
                ", username='" + username + '\'' +
                ", attributes=" + attributes +
                ", credentials=" + credentials +
                '}';
    }

    public static class Credentials {
        private String type;
        private String algorithm;
        private String salt;
        private String value;

        public Credentials() {
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getAlgorithm() {
            return algorithm;
        }

        public void setAlgorithm(String algorithm) {
            this.algorithm = algorithm;
        }

        public String getSalt() {
            return salt;
        }

        public void setSalt(String salt) {
            this.salt = salt;
        }

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }

        @Override
        public String toString() {
            return "Credentials{" +
                    "type='" + type + '\'' +
                    ", algorithm='" + algorithm + '\'' +
                    ", salt='" + salt + '\'' +
                    ", value='" + value + '\'' +
                    '}';
        }
    }

}
