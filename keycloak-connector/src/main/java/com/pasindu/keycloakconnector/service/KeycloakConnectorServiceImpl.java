package com.pasindu.keycloakconnector.service;

import com.pasindu.keycloakconnector.datamapper.LoginRequestToMultivaluedMapMapper;
import com.pasindu.keycloakconnector.resource.request.CreateUserRequest;
import com.pasindu.keycloakconnector.resource.request.LoginRequest;
import com.pasindu.keycloakconnector.resource.request.UserInfoRequest;
import com.pasindu.keycloakconnector.resource.response.LoginResponse;
import com.pasindu.keycloakconnector.resource.response.UserInfoResponse;
import com.pasindu.keycloakconnector.resource.response.UsersListItem;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.util.UriComponentsBuilder;
import reactor.core.publisher.Mono;

import java.util.List;

import static com.pasindu.keycloakconnector.datamapper.LoginRequestToMultivaluedMapMapper.map;

@Service
public class KeycloakConnectorServiceImpl implements KeycloakConnectorService {

    @Value("${keycloak.server.url}")
    private String keycloakServerUrl;

    private final WebClient.Builder webClientBuilder;

    public KeycloakConnectorServiceImpl(@Qualifier("webClientBuilder")
                                                WebClient.Builder clientBuilder) {
        this.webClientBuilder = clientBuilder;
    }


//    @Override
//    public LoginResponse token(String realmName, LoginRequest loginRequest) {
//
//        RestTemplate template = new RestTemplate();
//
//        HttpHeaders headers = new HttpHeaders();
//        headers.add("Content-Type", MediaType.APPLICATION_FORM_URLENCODED.toString());
//
//        MultiValueMap<String, String> map = map(loginRequest);
//
//        HttpEntity<MultiValueMap<String, String>> entity = new HttpEntity<MultiValueMap<String, String>>(map, headers);
//
//        String url = keycloakServerUrl + "/auth/realms/" + realmName + "/protocol/openid-connect/token";
//        ResponseEntity<LoginResponse> loginResponseBodyResponseEntity = template.postForEntity(url, entity, LoginResponse.class);
//        System.out.println(loginResponseBodyResponseEntity.getBody());
//
//        return loginResponseBodyResponseEntity.getBody();
//    }

    @Override
    public LoginResponse token(String realmName, LoginRequest loginRequest) {

        MultiValueMap<String, String> map = map(loginRequest);

        return webClientBuilder
                .build()
                .method(HttpMethod.POST)
                .uri(keycloakServerUrl + "/auth/realms/{realmName}/protocol/openid-connect/token"
                        , uriBuilder -> uriBuilder.build(realmName))
                .headers(h -> {
                            h.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
                        }
                )
                .accept(MediaType.APPLICATION_FORM_URLENCODED)
                .body(Mono.just(map), MultiValueMap.class)
                .retrieve()
                .onStatus(
                        s -> s.equals(HttpStatus.UNAUTHORIZED),
                        clientResponse -> Mono.just(new Exception("Not authenticated")))
                .onStatus(
                        HttpStatus::is4xxClientError,
                        clientResponse -> Mono.just(new
                                RuntimeException(clientResponse.statusCode().getReasonPhrase())))
                .onStatus(
                        HttpStatus::is5xxServerError,
                        clientResponse -> Mono.just(new Exception(clientResponse.statusCode().getReasonPhrase())))
                .bodyToMono(LoginResponse.class)
                .log()
                .block();

    }



    @Override
    public int createUser(String realmName, String authorizationHeader, CreateUserRequest createUserRequest) {

        RestTemplate template = new RestTemplate();

        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", MediaType.APPLICATION_JSON_VALUE);
        headers.add("Authorization", authorizationHeader);

        HttpEntity<CreateUserRequest> entity = new HttpEntity<>(createUserRequest, headers);

        String url = keycloakServerUrl + "/auth/admin/realms/" + realmName + "/users";
        ResponseEntity<String> createUserResponse = template.postForEntity(url, entity, String.class);

        return createUserResponse.getStatusCodeValue();
    }

    @Override
    public UserInfoResponse userInfo(String realmName, UserInfoRequest userInfoRequest) {

        RestTemplate template = new RestTemplate();

        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", MediaType.APPLICATION_FORM_URLENCODED.toString());

        MultiValueMap<String, String> map = new LinkedMultiValueMap<>();

        map.add("access_token", userInfoRequest.getAccess_token());
        map.add("client_id", userInfoRequest.getClient_id());
        map.add("client_secret", userInfoRequest.getClient_secret());

        HttpEntity<MultiValueMap<String, String>> entity = new HttpEntity<>(map, headers);

        System.out.println(userInfoRequest);

        String url = keycloakServerUrl + "/auth/realms/" + realmName + "/protocol/openid-connect/userinfo";
        ResponseEntity<UserInfoResponse> userInfoResponseResponseEntity = template.postForEntity(url, entity, UserInfoResponse.class);

        return userInfoResponseResponseEntity.getBody();
    }

    @Override
    public UsersListItem[] usersList(String authorizationHeader, String realmName, String email) {

        RestTemplate template = new RestTemplate();

        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", authorizationHeader);

        HttpEntity entity = new HttpEntity<>(headers);

        String baseUrl = keycloakServerUrl + "/auth/admin/realms/" + realmName + "/users";

        UriComponentsBuilder uriComponentsBuilder = UriComponentsBuilder.fromUriString(baseUrl);
        if (email != null)
            uriComponentsBuilder.queryParam("email", email);

        String url = uriComponentsBuilder.build().toUriString();


        ResponseEntity<UsersListItem[]> usersList = template.exchange(url, HttpMethod.GET, entity, UsersListItem[].class);

        return usersList.getBody();
    }

    @Override
    public int sendExecuteActionEmail(String authorizationHeader, String userId, List<String> actions, String realmName) {

        RestTemplate template = new RestTemplate();

        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", MediaType.APPLICATION_JSON_VALUE);
        headers.add("Authorization", authorizationHeader);

        HttpEntity<List<String>> entity = new HttpEntity<>(actions, headers);

        String url = keycloakServerUrl + "/auth/admin/realms/" + realmName + "/users/" + userId + "/execute-actions-email";
        ResponseEntity<String> emailSentResponse = template.exchange(url, HttpMethod.PUT, entity, String.class);

        return emailSentResponse.getStatusCodeValue();
    }
}
