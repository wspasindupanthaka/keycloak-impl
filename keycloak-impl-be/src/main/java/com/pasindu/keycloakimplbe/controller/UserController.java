package com.pasindu.keycloakimplbe.controller;

import com.pasindu.keycloakimplbe.resource.CreateUser;
import com.pasindu.keycloakimplbe.resource.CustomResponse;
import com.pasindu.keycloakimplbe.resource.ResetPassword;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@RequestMapping("/user")
public interface UserController {

    @RequestMapping(value = "", method = RequestMethod.POST)
    ResponseEntity<CustomResponse> createUser(@RequestHeader(value = "Authorization") String authorizationHeader,
                                              @RequestBody CreateUser createUser);

    @RequestMapping(value = "/loggedin-user", method = RequestMethod.GET)
    ResponseEntity<String> getLoggedInUser(@RequestHeader(value = "Authorization") String authorizationHeader);


    @RequestMapping(value = "/password/change", method = RequestMethod.PUT)
    ResponseEntity<CustomResponse> changePassword(@RequestHeader(value = "Authorization") String authorizationHeader);

    @RequestMapping(value = "/password/reset", method = RequestMethod.POST)
    ResponseEntity<CustomResponse> resetPassword(@RequestBody ResetPassword resetPassword);

}
