package com.pasindu.keycloakimplbe.controller.impl;

import com.pasindu.keycloakimplbe.controller.UserController;
import com.pasindu.keycloakimplbe.resource.CreateUser;
import com.pasindu.keycloakimplbe.resource.CustomResponse;
import com.pasindu.keycloakimplbe.resource.ResetPassword;
import com.pasindu.keycloakimplbe.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserControllerImpl implements UserController {

    @Autowired
    private UserService userService;

    @Override
    public ResponseEntity<CustomResponse> createUser(@RequestHeader(value = "Authorization") String authorizationHeader,
                                                     @RequestBody CreateUser createUser) {
        CustomResponse customResponse = userService.createUser(authorizationHeader, createUser);
        return ResponseEntity.status(HttpStatus.CREATED).body(customResponse);
    }

    @Override
    public ResponseEntity<String>getLoggedInUser(@RequestHeader(value = "Authorization") String authorizationHeader) {
        String  userName= userService.getLoggedInUser(authorizationHeader);
        return ResponseEntity.status(HttpStatus.CREATED).body(userName);
    }

    @Override
    public ResponseEntity<CustomResponse> changePassword(@RequestHeader(value = "Authorization") String authorizationHeader) {
        CustomResponse customResponse = userService.changePassword(authorizationHeader);
        return ResponseEntity.status(HttpStatus.CREATED).body(customResponse);
    }

    @Override
    public ResponseEntity<CustomResponse> resetPassword(@RequestBody ResetPassword resetPassword) {
        CustomResponse customResponse = userService.resetPassword(resetPassword);
        return ResponseEntity.status(HttpStatus.CREATED).body(customResponse);
    }

}
