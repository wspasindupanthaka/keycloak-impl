package com.pasindu.keycloakimplbe.service.impl;

import com.pasindu.keycloakimplbe.resource.*;
import com.pasindu.keycloakimplbe.response.UserInfoResponse;
import com.pasindu.keycloakimplbe.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import javax.servlet.http.HttpServletRequest;
import java.util.Arrays;
import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    @Value("${connector.url}")
    private String keycloakConnectorUrl;

    @Value("${keycloak.realm}")
    private String keycloakRealm;

    @Value("${keycloak.resource}")
    private String keycloakResource;

    @Value("${keycloak.credentials.secret}")
    private String keycloakCredentialsSecret;

    @Value("${admin.username}")
    private String keycloakAdminUsername;

    @Value("${admin.password}")
    private String keycloakAdminPassword;

    private final HttpServletRequest request;

    public UserServiceImpl(HttpServletRequest request) {
        this.request = request;
    }

    @Override
    public String getLoggedInUser(String authorizationHeader) {
        return request.getUserPrincipal().getName();
    }

    public LoginResponse token(LoginRequest loginRequest) {

        RestTemplate template = new RestTemplate();

        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", MediaType.APPLICATION_FORM_URLENCODED.toString());

        MultiValueMap<String, String> map = new LinkedMultiValueMap<>();

        if (loginRequest.REFRESH_TOKEN.equals(loginRequest.getGrant_type())) {
            map.add("grant_type", loginRequest.REFRESH_TOKEN);
            map.add("refresh_token", loginRequest.getRefresh_token());
        } else {
            map.add("grant_type", loginRequest.PASSWORD);
            map.add("username", loginRequest.getUsername());
            map.add("password", loginRequest.getPassword());
        }


        map.add("client_id", keycloakResource);
        map.add("client_secret", keycloakCredentialsSecret);

        HttpEntity<MultiValueMap<String, String>> entity = new HttpEntity<MultiValueMap<String, String>>(map, headers);

        String url = keycloakConnectorUrl + "/auth/realms/" + keycloakRealm + "/protocol/openid-connect/token";
        ResponseEntity<LoginResponse> loginResponseBodyResponseEntity = template.postForEntity(url, entity, LoginResponse.class);

        return loginResponseBodyResponseEntity.getBody();
    }

    @Override
    public CustomResponse createUser(String authorizationHeader, CreateUser createUser) {

        RestTemplate template = new RestTemplate();

        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", MediaType.APPLICATION_JSON_VALUE);
        headers.add("Authorization", authorizationHeader);

        HttpEntity<CreateUser> entity = new HttpEntity<>(createUser, headers);

        String url = keycloakConnectorUrl + "/auth/admin/realms/" + keycloakRealm + "/users";
        ResponseEntity<CustomResponse> customResponseResponseEntity = template.postForEntity(url, entity, CustomResponse.class);

        return customResponseResponseEntity.getBody();

    }

    @Override
    public CustomResponse changePassword(String authorizationHeader) {

        /**
         Get userId from validateUser
         Login with Admin User
         Invoke executeActionsEmail endpoint
         */

        UserInfoResponse loggedInUserInfo = getUserInfo(authorizationHeader);
        if (loggedInUserInfo != null) {
            LoginResponse adminUserToken = token(new LoginRequest(keycloakAdminUsername, keycloakAdminPassword, LoginRequest.PASSWORD));
            if (adminUserToken != null) {
                String adminUserTokenAccessToken = adminUserToken.getAccess_token();
                CustomResponse updatePasswordResponse = sendExecuteActionsEmail(loggedInUserInfo.getSub(), adminUserTokenAccessToken, Arrays.asList("UPDATE_PASSWORD"));
            }
        }

        return null;

    }

    @Override
    public CustomResponse resetPassword(ResetPassword resetPassword) {

        /**
         * login with admin user
         * filter user from email (to get user id)
         * send executeActions email
         */

        LoginResponse adminUserToken = token(new LoginRequest(keycloakAdminUsername, keycloakAdminPassword, LoginRequest.PASSWORD));

        if (adminUserToken != null) {
            UsersListItem[] usersListItems = usersList(adminUserToken.getAccess_token(), keycloakRealm, resetPassword.getEmail());
            if (usersListItems.length > 0) {
                UsersListItem userWithEmail = usersListItems[0];
                if (userWithEmail != null) {
                    String userId = userWithEmail.getId();
                    CustomResponse resetPasswordResponse = sendExecuteActionsEmail(userId, adminUserToken.getAccess_token(), Arrays.asList("UPDATE_PASSWORD"));
                    return resetPasswordResponse;
                }
            }
        }
        return null;

    }

    public UsersListItem[] usersList(String authorizationHeader, String realmName, String email) {

        RestTemplate template = new RestTemplate();

        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Bearer " + authorizationHeader);

        HttpEntity entity = new HttpEntity<>(headers);

        String baseUrl = keycloakConnectorUrl + "/auth/admin/realms/" + realmName + "/users";

        UriComponentsBuilder uriComponentsBuilder = UriComponentsBuilder.fromUriString(baseUrl);
        if (email != null)
            uriComponentsBuilder.queryParam("email", email);

        String url = uriComponentsBuilder.build().toUriString();

        ResponseEntity<UsersListItem[]> usersList = template.exchange(url, HttpMethod.GET, entity, UsersListItem[].class);

        return usersList.getBody();
    }

    private CustomResponse sendExecuteActionsEmail(String userId, String authorizationHeader, List<String> actions) {
        RestTemplate template = new RestTemplate();

        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", MediaType.APPLICATION_JSON_VALUE);
        headers.add("Authorization", "Bearer " + authorizationHeader);

        HttpEntity<List<String>> entity = new HttpEntity<>(actions, headers);

        String url = keycloakConnectorUrl + "/auth/admin/realms/" + keycloakRealm + "/users/" + userId + "/execute-actions-email";
        ResponseEntity<CustomResponse> emailSentResponse = template.exchange(url, HttpMethod.PUT, entity, CustomResponse.class);

        return emailSentResponse.getBody();
    }

    private LoginResponse loginAsAdmin() {
        return token(new LoginRequest(keycloakAdminUsername, keycloakAdminPassword, LoginRequest.PASSWORD));
    }

    private UserInfoResponse getUserInfo(String authorizationHeader) {
        RestTemplate template = new RestTemplate();

        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", MediaType.APPLICATION_FORM_URLENCODED.toString());

        MultiValueMap<String, String> map = new LinkedMultiValueMap<>();

        map.add("access_token", authorizationHeader.replace("Bearer ", ""));
        map.add("client_id", keycloakResource);
        map.add("client_secret", keycloakCredentialsSecret);

        HttpEntity<MultiValueMap<String, String>> entity = new HttpEntity<>(map, headers);

        String url = keycloakConnectorUrl + "/auth/realms/" + keycloakRealm + "/protocol/openid-connect/userinfo";
        ResponseEntity<UserInfoResponse> userInfoResponseResponseEntity = template.postForEntity(url, entity, UserInfoResponse.class);

        return userInfoResponseResponseEntity.getBody();
    }


//    public static void main(String[] args) {
//        UserInfoResponse userInfo = new UserServiceImpl().getUserInfo("eyJhbGciOiJSUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJsMGx3ek1TdGhWWjkzQm04YkQxbDRucjlELU55V3EzQjkxNC1naWZOQWpFIn0.eyJleHAiOjE2MjQxODI0MjksImlhdCI6MTYyNDE4MjEyOSwianRpIjoiZDRjOTA1NDctOTU3MC00OWExLWE0OTUtMDMzNzUyNGJlMjQ2IiwiaXNzIjoiaHR0cDovL2xvY2FsaG9zdDo5MDkwL2F1dGgvcmVhbG1zL0FsbGlhbnoiLCJhdWQiOlsicmVhbG0tbWFuYWdlbWVudCIsIm15YWxsaWFueiJdLCJzdWIiOiI5NTg2NGRjNS1jMjQ0LTQxMGQtYjQ5ZC01ODk3MWM3NTA0MDciLCJ0eXAiOiJCZWFyZXIiLCJhenAiOiJrZXljbG9hay1pbXBsLWJlIiwic2Vzc2lvbl9zdGF0ZSI6ImEzM2JiNTRmLWJlMjYtNDAyMS1hY2JiLWU5MjI1ZTZiNjJiMyIsImFjciI6IjEiLCJyZWFsbV9hY2Nlc3MiOnsicm9sZXMiOlsib2ZmbGluZV9hY2Nlc3MiLCJ1bWFfYXV0aG9yaXphdGlvbiJdfSwicmVzb3VyY2VfYWNjZXNzIjp7InJlYWxtLW1hbmFnZW1lbnQiOnsicm9sZXMiOlsibWFuYWdlLXVzZXJzIl19LCJteWFsbGlhbnoiOnsicm9sZXMiOlsibXlhbGxpYW56Il19fSwic2NvcGUiOiJwcm9maWxlIGVtYWlsIiwiZW1haWxfdmVyaWZpZWQiOnRydWUsIm5hbWUiOiJQYXNpbmR1IFBhbnRoYWthIiwicHJlZmVycmVkX3VzZXJuYW1lIjoicGFzaW5kdSIsImdpdmVuX25hbWUiOiJQYXNpbmR1IiwiZmFtaWx5X25hbWUiOiJQYW50aGFrYSJ9.YDqYBPp_YfnYKlx2H1uwuYEXsPE0YBVahGfeOBh4menLDVeeoOjfzi-3tBGvtxCcb48VqItQk4Usd4qTupgq9UQArf2dv7N8McHm2kj0se_uyeS71e7Q3vb9TIjWf4VOX8A7Am1Wrl4NtNlW7oWcCMeB7hky2gzx7zC2L9A3Isa1zncq8pEIH-ljuCxIDOuWipgQSa5mDHL-HI0gkDJqKLkmybWvGZLX6hZ2iLrxWO1DkJgb428befDNJfoKItM-DhzWZQmz_XqgFiuZaPB5plWBpA6KDRbSGKx03cb2dkxdSMWesAZ54lHpbQF5Ad9AXtCYQr2ILvhAF2Rba8Dwww");
//        System.out.println(userInfo);
//
//    }
}
