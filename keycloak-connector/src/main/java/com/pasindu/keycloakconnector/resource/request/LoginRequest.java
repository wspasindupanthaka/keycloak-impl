package com.pasindu.keycloakconnector.resource.request;

public class LoginRequest {

    private String client_id;
    private String client_secret;
    private String username;
    private String password;
    private String scope;
    private String grant_type;
    private String refresh_token;

    public static final String PASSWORD = "password";
    public static final String REFRESH_TOKEN = "refresh_token";

    public LoginRequest() {
    }



    public void setClient_id(String client_id) {
        this.client_id = client_id;
    }

    public void setClient_secret(String client_secret) {
        this.client_secret = client_secret;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setScope(String scope) {
        this.scope = scope;
    }


    public String getClient_id() {
        return client_id;
    }

    public String getClient_secret() {
        return client_secret;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public String getScope() {
        return scope;
    }

    public String getGrant_type() {
        return grant_type;
    }

    public void setGrant_type(String grant_type) {
        this.grant_type = grant_type;
    }

    public String getRefresh_token() {
        return refresh_token;
    }

    public void setRefresh_token(String refresh_token) {
        this.refresh_token = refresh_token;
    }
}
