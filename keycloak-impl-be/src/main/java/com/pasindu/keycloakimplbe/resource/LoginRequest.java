package com.pasindu.keycloakimplbe.resource;

public class LoginRequest {

    private String username;
    private String password;
    private String grant_type;
    private String refresh_token;

    public static final String PASSWORD = "password";
    public static final String REFRESH_TOKEN = "refresh_token";

    public LoginRequest() {
    }

    public LoginRequest(String username, String password, String grant_type) {
        this.username = username;
        this.password = password;
        this.grant_type = grant_type;
    }

    public LoginRequest(String refresh_token, String grant_type) {
        this.grant_type = grant_type;
        this.refresh_token = refresh_token;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public String getGrant_type() {
        return grant_type;
    }

    public void setGrant_type(String grant_type) {
        this.grant_type = grant_type;
    }

    public String getRefresh_token() {
        return refresh_token;
    }

    public void setRefresh_token(String refresh_token) {
        this.refresh_token = refresh_token;
    }
}
